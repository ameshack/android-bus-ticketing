package moderncoastexp.mt.co.ke;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;


public class BusList extends ActionBarActivity{
	// All static variables
	// static final String URL = "http://168.63.97.186/kwani/mkwani7/videos";
	static final String URL = "http://178.238.232.121/tms/admins/schedulesjson/";
    SimpleDateFormat sdf2=new SimpleDateFormat("dd-MM-yyyy");	
    SimpleDateFormat sdf1=new SimpleDateFormat("EEE , d MMM , yyyy");
	 
	static final String KEY_busname = "busname"; // parent node
	static final String KEY_busdeptime = "deptime";
	static final String KEY_busreptime = "reptime";
	static final String KEY_routecode = "routecode";
	static final String KEY_Traveldate = "traveldate";
	static final String KEY_busId = "busid";
	static final String KEY_seatlist = "seatlist";
	static final String KEY_bizprice = "bizprice";
	static final String KEY_fclasprice = "fclasprice";
	static final String KEY_vipsprice = "vipprice";
	//Mombasa:Nairobi:08-08-2013

	private LayoutInflater mInflater;
	ListView list;

	BusListAdapter adapter;
	
	String JsonText;
	 JSONArray mJsonArray;
	 String theTravelDate;
	 String FromTown;
	 String ToTown;
	 String datethis;
	//View viewer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		setContentView(R.layout.mainbuslist);
		
		  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		  getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
		
		theTravelDate = getIntent().getSerializableExtra("thidate").toString();
		FromTown = getIntent().getSerializableExtra("FromTown").toString();
		ToTown = getIntent().getSerializableExtra("ToTown").toString();
		  try{
			  datethis= sdf2.format((Date) sdf1.parse(theTravelDate));
	          }
	          catch (ParseException e) {
	              System.out.println("Exception :" + e);
	          }
		 
		  new MyAsyncTask().execute();
	}

	/*@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		viewer = setElecViewHome(inflater, container);

		return viewer;
	}
*/

	


	public void loadUI() throws JSONException {

		ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
		
		String myBizseats="";
		String myFclassseats="";

		
		 
		try {
			mJsonArray = new JSONArray(JsonText);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  JSONObject mJsonObject = new JSONObject();
		  
		  if(mJsonArray.length()<1)
		  {
			  new myconfigs().showToastMe(this,"Oops! there were no bus available at this time");
			  finish();
			  
		  }
		
		// looping through all song nodes <song>
		for (int i = 0; i < mJsonArray.length(); i++) {
			// creating new HashMap
			HashMap<String, String> map = new HashMap<String, String>();
			mJsonObject = mJsonArray.getJSONObject(i);
			

			map.put(KEY_busname, mJsonObject.getString("busname"));
			map.put(KEY_Traveldate,datethis);
			map.put(KEY_routecode, mJsonObject.getString("routecode"));
			map.put(KEY_busdeptime, mJsonObject.getString("deptime"));
			map.put(KEY_busreptime, mJsonObject.getString("reptime"));
			map.put(KEY_busId, mJsonObject.getString("busid"));
			map.put(KEY_bizprice, mJsonObject.getString("bizprice"));
			map.put(KEY_fclasprice, mJsonObject.getString("fclassprice"));
			map.put(KEY_vipsprice, mJsonObject.getString("vipprice"));
			
			if(mJsonObject.getString("bizseats").length()>0){
				myBizseats=mJsonObject.getString("bizseats")+",";
			}
					
			if(mJsonObject.getString("fclassseats").length()>0){
				myFclassseats=","+mJsonObject.getString("fclassseats");
			}
			
		
			
			map.put(KEY_seatlist, myBizseats+ mJsonObject.getString("vipseats")+myFclassseats);
	
			

			songsList.add(map);

		}

		list = (ListView)findViewById(R.id.listeven);

		// Getting adapter by passing xml data ArrayList
		adapter = new BusListAdapter(this, songsList);
		list.setAdapter(adapter);

		// Click event for single list row
		list.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
		
				
			Intent intent = new Intent(view.getContext(), NormalSeats.class);
			
		    TextView BusnameView =(TextView)view.findViewById(R.id.titleforum);		    
				String Busname = BusnameView.getText().toString();
			TextView deptimeView =(TextView)view.findViewById(R.id.artistforum);		    
					String deptime = deptimeView.getText().toString();
					
            TextView travelDateView =(TextView)view.findViewById(R.id.kwanibukprice);		    
					String datetravel = travelDateView.getText().toString();
					
			TextView reptimeView =(TextView)view.findViewById(R.id.titleforumID);		    
					String reptime = reptimeView.getText().toString();
					
		    TextView busidView =(TextView)view.findViewById(R.id.countcomments);		    
					String busid = busidView.getText().toString();
					
		    TextView seatlistView =(TextView)view.findViewById(R.id.countlikes);		    
							String seatlist = seatlistView.getText().toString();
							
			TextView bizpriceView =(TextView)view.findViewById(R.id.countdislikes);		    
							String bizprice = bizpriceView.getText().toString();
							
			TextView fclasspriceView =(TextView)view.findViewById(R.id.kissimageurl);		    
							String fclassprice = fclasspriceView.getText().toString();
							
		    TextView vippriceView =(TextView)view.findViewById(R.id.vipprice);		    
							String vipprice = vippriceView.getText().toString();
				
				
				intent.putExtra("busname",Busname );
				intent.putExtra("deptime",deptime );
				intent.putExtra("datetravel",datetravel );
				intent.putExtra("reptime",reptime );
				intent.putExtra("busid",busid );
				intent.putExtra("seatlist",seatlist );
				intent.putExtra("bizprice",bizprice );
				intent.putExtra("fclassprice",fclassprice );
				intent.putExtra("vipprice",vipprice );
				intent.putExtra("from",FromTown );
				intent.putExtra("to",ToTown );
				startActivity(intent);

			}
		});

	}

	private class MyAsyncTask extends AsyncTask<Void, Void, Void> {
		ProgressBar progressBar;

		@Override
		protected void onPostExecute(Void result) {
			progressBar = (ProgressBar)findViewById(R.id.progressevent);

			progressBar.setVisibility(View.INVISIBLE);
			try {
				loadUI();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected Void doInBackground(Void... params) {
			// your network operation

			JsonText = new myconfigs().getJsonFromUrl(URL+FromTown+":"+ToTown+":"+datethis);

			return null;
		}
	}
	
	



}