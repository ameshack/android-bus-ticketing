package moderncoastexp.mt.co.ke;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;

public class MainActivity extends ActionBarActivity {
	  private Thread mSplashThread;    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
		
		
		   mSplashThread =  new Thread(){
		        @Override
		        public void run(){
		            try {
		                synchronized(this){
		                    // Wait given period of time or exit on touch
		                	
		                	
		                    wait(3000);
		                }
		            }
		            catch(InterruptedException ex){                    
		            }

		            // Run next activity
		            validateUse();
		           
		            finish();
		           // stop();                    
		        }
		    };
		    
		    mSplashThread.start(); 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void validateUse()
    {
      
        
    	 Intent intent = new Intent();
         intent.setClass(this, LoginLoyal.class);
        
         startActivity(intent);
    }

}
