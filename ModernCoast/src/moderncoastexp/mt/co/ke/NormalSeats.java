package moderncoastexp.mt.co.ke;

import java.util.Arrays;
import java.util.List;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class NormalSeats extends ActionBarActivity{
	
	
	   String [] seatArray;
	   String [] seatArraydirty;
	   String AvailableSeats;
	   
		 String busname; 
		 String busdeptime;
		 String busreptime;
		 String routecode;
		 String Traveldate;
		 String busId;
	     String seatlist;
		 String bizprice;
		 String fclasprice ;
		 String vipsprice;
		 String SeatPrefix,Bfrom,Bto;
	  
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	       
	       busname = getIntent().getSerializableExtra("busname").toString();
	       
	       if(busname.equalsIgnoreCase("Normal Bus")){
	    	   setContentView(R.layout.normalseats);
	    	   SeatPrefix="seat";
	    	   
	       }
	       
	       else  if(busname.equalsIgnoreCase("Oxygen Bus")){
	    	   setContentView(R.layout.oxygeneats);
	    	   SeatPrefix="seatoxy";
	    	   
	       }
	        
	       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	       getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
	    
	    //  loadSeatView();
	       
	      
	       busdeptime = getIntent().getSerializableExtra("deptime").toString();
	       busreptime = getIntent().getSerializableExtra("reptime").toString();
	      // routecode = getIntent().getSerializableExtra("thidate").toString();
	       Traveldate = getIntent().getSerializableExtra("datetravel").toString();
	       busId = getIntent().getSerializableExtra("busid").toString();
	       seatlist = getIntent().getSerializableExtra("seatlist").toString();
	       bizprice = getIntent().getSerializableExtra("bizprice").toString();
	       fclasprice = getIntent().getSerializableExtra("fclassprice").toString();
	       vipsprice = getIntent().getSerializableExtra("vipprice").toString();
	       Bfrom = getIntent().getSerializableExtra("from").toString();
	       Bto = getIntent().getSerializableExtra("to").toString();
	        
	       loadSeatView();
	      
	             
	  //2131296264
	        
	        
	   }
	   
	   
	   public void bookSeat(View view)
	   {
		   
		   Button b = (Button)view;
		   String Seattext = b.getText().toString();
		   
		   List <String> Seatlist = Arrays.asList(seatArray);
		   
		   if(Seatlist.contains(Seattext)) 
		   { 
			  // new myconfigs().showToastMe(this,Seattext);
			   Intent intent = new Intent(view.getContext(), payments.class);
			    intent.putExtra("busname",busname );
				intent.putExtra("deptime",busdeptime );
				intent.putExtra("datetravel",Traveldate );
				intent.putExtra("reptime",busreptime );
				intent.putExtra("busid",busId );
				intent.putExtra("seatlist",seatlist );
				intent.putExtra("bizprice",bizprice );
				intent.putExtra("fclassprice",fclasprice );
				intent.putExtra("vipprice",vipsprice );
				intent.putExtra("seatno",Seattext );
				intent.putExtra("seatype",""+b.getTag() );
			
				intent.putExtra("from",Bfrom );
				intent.putExtra("to",Bto );
			   startActivity(intent);
		   }
		   else
		   {
			   new myconfigs().showToastMe(this,"This seat is booked"); 
		   }
		   
		   
	   }
	   public void loadSeatView()
	   {
		   
		  // new myconfigs().showToastMe(this,AvailableSeats);
		  // String SeatsServer="1,2,3,4,5,6,7,9,10,11,12,13,14,15";
		   
		  String SeatsServer=seatlist.replaceAll(" Vip", "").replaceAll(" Fcls", "").replaceAll(" ", "");
		  seatArraydirty=seatlist.split(",");
		   
		    seatArray=SeatsServer.split(",");
		   
		 
		   
		  for(int x = 0; x < seatArray.length; x = x+1) 
		   {
			   
	            int resID = getResources().getIdentifier(SeatPrefix+seatArray[x].trim(), "id", "moderncoastexp.mt.co.ke");
		        
		       Button   btnseat= (Button)findViewById(resID);
		       btnseat.setTag(seatArraydirty[x]);
		       //btnseat.setBackgroundColor(Color.BLUE);
		       btnseat.setBackgroundResource(R.drawable.ic_key_seat_available);
		       btnseat.setEnabled(true);
		   } 
	   }
	   
	   


}
