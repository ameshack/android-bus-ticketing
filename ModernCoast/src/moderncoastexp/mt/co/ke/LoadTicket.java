package moderncoastexp.mt.co.ke;



import moderncoastexp.mt.co.ke.server.GlobalData;
import moderncoastexp.mt.co.ke.server.SeverSender;
import moderncoastexp.mt.co.ke.server.myconstants;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;


public class LoadTicket extends ActionBarActivity{
	

	//private UserDataSource datasource;
	//GlobalData person;
	private ProgressBar spinner;
	Button connectButton;
	GlobalData person;
	
	 String busname; 
	 String busdeptime;
	 String busreptime;
	 String routecode;
	 String Traveldate;
	 String busId;
     String seatlist;
	 String bizprice;
	 String fclasprice ;
	 String vipsprice;
	 String SeatPrefix,seatNumber,Bfrom,Bto,seatType,CustomerClas,FinalPrice,mpesaref,
	 customerphone;
	 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loadersuv);
				
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
		//datasource = new UserDataSource(this);
	   // datasource.open();
	    
	       spinner = (ProgressBar)findViewById(R.id.progressBar1);
	    
	       busdeptime = getIntent().getSerializableExtra("deptime").toString();
	       busreptime = getIntent().getSerializableExtra("reptime").toString();
	       busname = getIntent().getSerializableExtra("busname").toString();
	      // routecode = getIntent().getSerializableExtra("thidate").toString();
	       Traveldate = getIntent().getSerializableExtra("datetravel").toString();
	       busId = getIntent().getSerializableExtra("busid").toString();
	       seatlist = getIntent().getSerializableExtra("seatlist").toString();
	       seatNumber = getIntent().getSerializableExtra("seatno").toString();
	       bizprice = getIntent().getSerializableExtra("bizprice").toString();
	       fclasprice = getIntent().getSerializableExtra("fclassprice").toString();
	       vipsprice = getIntent().getSerializableExtra("vipprice").toString();
	       FinalPrice = getIntent().getSerializableExtra("finalprice").toString();
	       mpesaref = getIntent().getSerializableExtra("mpesaref").toString();
	       customerphone = getIntent().getSerializableExtra("phone").toString();
	       Bfrom = getIntent().getSerializableExtra("from").toString();
	       Bto = getIntent().getSerializableExtra("to").toString();
	       seatType = getIntent().getSerializableExtra("seatype").toString();
	    
	    SetTicketDetails();
	    
	    IsdeviceConnected();
	    Connectbutton();
	
	 
	
		
	
	}
	
	public void SetTicketDetails()
	{
		person = new GlobalData();    
		
		//person.setamount(Integer.parseInt(FinalPrice));
		person.setamount(FinalPrice);
		person.setfrom(Bfrom);
		person.setto(Bto);
		person.setmpesaref(mpesaref);
	//	person.setseatno(Integer.parseInt(seatNumber));
		person.setseatno(seatNumber);
		person.setcardno(" ");
		person.setvouchernumbr(myconstants.userloyaltyNO);
		//person.setscheduleid(Integer.parseInt(busId));
		person.setscheduleid(busId);
		person.setUserphone(customerphone);
	}
	
	public void IsdeviceConnected()
	{
		   if(new TestInternet().isOnline(this))
		     {
			   new HttpAsyncTask().execute(myconstants.ServerIP); 
		    	
		     }	
		   else{
			   load(); 
		   }
	}
	
	public void Connectbutton() {
		 
		connectButton = (Button) findViewById(R.id.connect);
 
		connectButton.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				   if(new TestInternet().isOnline(v.getContext()))
				     {
					   unload();
					   new HttpAsyncTask().execute(myconstants.ServerIP); 
				    	
				     }
				   else{
					   showToast("Error: No connection");
				   }
			 
 
			}
 
		});
 
	}
	
	public void load(){
	      spinner.setVisibility(View.GONE);
	   }
	
	public void unload(){
	      spinner.setVisibility(View.VISIBLE);
	   }
	
	  private class HttpAsyncTask extends AsyncTask<String, Void, String> {
			
		  
	        @Override
	        protected String doInBackground(String... urls) {
	 
	        	
	          
	            return new SeverSender().PostTicket(urls[0],person);
	        }
	        // onPostExecute displays the results of the AsyncTask.
	        @Override
	        protected void onPostExecute(String result) {
	        	
	        //	datasource.getAllUserDet(person);
    	    
	        	//System.out.println(myconstants.Useralldetljson);
	        	showToast("Survey submited "+result);
	           
	            
	 	 			  Intent intent = new Intent(LoadTicket.this, PayDetail.class);
	 				    intent.putExtra("busname",busname );
	 					intent.putExtra("deptime",busdeptime );
	 					intent.putExtra("datetravel",Traveldate );
	 					intent.putExtra("reptime",busreptime );
	 					intent.putExtra("busid",busId );
	 					intent.putExtra("seatlist",seatlist );
	 					intent.putExtra("bizprice",bizprice );
	 					intent.putExtra("fclassprice",fclasprice );
	 					intent.putExtra("vipprice",vipsprice );
	 					intent.putExtra("seatno",seatNumber );
	 					intent.putExtra("seatype",seatType);
	 					intent.putExtra("finalprice",FinalPrice);
	 					intent.putExtra("JsonTsetail",result);
	 					intent.putExtra("mpesaref",mpesaref);
	 					intent.putExtra("phone",person.getUserphone());
	 					intent.putExtra("from",Bfrom );
	 					intent.putExtra("to",Bto );
	 				   startActivity(intent);
	 	 
	 	 			   finish();
	            	
	 	 			
    	
	       }
	    }
	
	  
	  public void showToast(String text){
		  Toast.makeText(this,text,
		           Toast.LENGTH_LONG).show();
	 }

}
