package moderncoastexp.mt.co.ke;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BusListAdapter extends BaseAdapter {

	private FragmentActivity activity;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater = null;
	
	String mime = "text/html";
	String encoding = "utf-8";
	SimpleDateFormat sdf=new SimpleDateFormat("EEE , MMM d , yyyy");
	SimpleDateFormat sdf1=new SimpleDateFormat("dd-MM-yyyy");
	Date datethis;

	public BusListAdapter(FragmentActivity a, ArrayList<HashMap<String, String>> d) {
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	//	imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.buslist_row, null);

		TextView titlepost = (TextView) vi.findViewById(R.id.titleforum); // title
																			// of
																			// the
																			// post
		TextView nametimepostweb = (TextView) vi
				.findViewById(R.id.artistforum);
		//departure time
		TextView deptureTime = (TextView) vi.findViewById(R.id.kwanibukprice);
		TextView seatlist = (TextView) vi.findViewById(R.id.countlikes);
		TextView bizprice = (TextView) vi.findViewById(R.id.countdislikes);
		TextView fclsprice = (TextView) vi.findViewById(R.id.kissimageurl);
		TextView vipprice = (TextView) vi.findViewById(R.id.vipprice);

		//nametimepostweb.getSettings().setJavaScriptEnabled(true);
		//nametimepostweb.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		//nametimepostweb.getSettings().setDomStorageEnabled(true);

		// poster and time to post
		TextView titleforumID = (TextView) vi.findViewById(R.id.titleforumID);
		final TextView thebusID = (TextView) vi.findViewById(R.id.countcomments);
		// TextView kwanibukprice =
		// (TextView)vi.findViewById(R.id.kwanibukprice);

		ImageView thumb_image = (ImageView) vi
				.findViewById(R.id.listforum_image); // thumb image

		HashMap<String, String> song = new HashMap<String, String>();
		song = data.get(position);

		// Setting all values in listview
		titlepost.setText(song.get(BusList.KEY_busname)+" Bus");
		nametimepostweb.setText("Departure Time: "+song.get(BusList.KEY_busdeptime));
		titleforumID.setText(song.get(BusList.KEY_busreptime));
		thebusID.setText(song.get(BusList.KEY_busId));
		seatlist.setText(song.get(BusList.KEY_seatlist));
		bizprice.setText(song.get(BusList.KEY_bizprice));
		fclsprice.setText(song.get(BusList.KEY_fclasprice));
		vipprice.setText(song.get(BusList.KEY_vipsprice));
		
		try {
			datethis = (Date) sdf1.parse(song.get(BusList.KEY_Traveldate).trim());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		deptureTime.setText(sdf.format(datethis));
		//titleforumID.setText(song.get(BusList.KEY_ID));
		
		//thebodytext.setText(song.get(BusList.KEY_content));
	
		
		
		
		
	/*	nametimepostweb.setOnTouchListener(new View.OnTouchListener() {
			// The definition of your actual onClick-type method
			public boolean onTouch(View v, MotionEvent event) {
				// Switching on what type of touch it was
				switch (event.getAction()) {
				// ACTION_UP and ACTION_DOWN together make up a click
				// We're handling both to make sure we grab it
				case MotionEvent.ACTION_DOWN:
			
				case MotionEvent.ACTION_UP: {

					Intent intent = new Intent(v.getContext(), myWebView.class);
					// TextView countcomments =
					// (TextView)v.findViewById(R.id.countcomments);
					String contentNo = thebodytext.getText().toString();
					intent.putExtra("thiscontent", "<html><body>" + contentNo
							+ "</body></html>");
					activity.startActivity(intent);
					// startActivityForResult(intent, 0);
					// The code will pause here until you exit the new Activity
					// It will then go back to what it was doing
					// In our case, waiting for more input
					break;
				}
				}

				// Returning false means that we won't be handling any other
				// input
				// Any un-handled gestures are tossed out
				return false;
			}

		});
		
		*/
		
		// /nametimepost.setText("by "+song.get(Elections.KEY_by));
		
		// kwanibukprice.setText(song.get(Elections.KEY_price));

	//	imageLoader.DisplayImage(song.get(BusList.KEY_thumb), thumb_image);

		return vi;
	}
}