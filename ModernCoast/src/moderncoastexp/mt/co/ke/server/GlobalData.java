package moderncoastexp.mt.co.ke.server;

import org.json.JSONArray;

public class GlobalData {
	

	
	private String UserEmail;
	private String UserFname;
	private String UserLname;
	private String rescode;
	private String Userphone,from,to,cardno,mpesaref;
	private String amount,seatno,scheduleid,vouchernumbr;
	private int Userid;
	private JSONArray liststore;
	
	public String getUserEmail()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.UserEmail;
	}
	public void setUserEmail(String value)
	{
	     
	     this.UserEmail = value;
	}
	
	public String getUserFname()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.UserFname;
	}
	public void setUserFname(String value)
	{
	     
	     this.UserFname = value;
	}
	
	public String getUserLname()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.UserLname;
	}
	public void setUserLname(String value)
	{
	     
	     this.UserLname = value;
	}
	
	
	public String getUserphone()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.Userphone;
	}
	public void setUserphone(String value)
	{
	     
	     this.Userphone = value;
	}
	
	public int getUserid()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.Userid;
	}
	public void setUserid(int value)
	{
	     
	     this.Userid = value;
	}
	
	public String getrescode()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.rescode;
	}
	public void setrescode(String value)
	{
	     
	     this.rescode = value;
	}

	public JSONArray getliststore()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.liststore;
	}
	public void setliststore(JSONArray value)
	{
	     
	     this.liststore = value;
	}
	
	
	public String getamount()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.amount;
	}
	public void setamount(String value)
	{
	     
	     this.amount = value;
	}
	
	public String getseatno()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.seatno;
	}
	public void setseatno(String value)
	{
	     
	     this.seatno = value;
	}
	
	public String getscheduleid()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.scheduleid;
	}
	public void setscheduleid(String value)
	{
	     
	     this.scheduleid = value;
	}

	public String getfrom()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.from;
	}
	public void setfrom(String value)
	{
	     
	     this.from = value;
	}

	public String getto()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.to;
	}
	public void setto(String value)
	{
	     
	     this.to = value;
	}

	
	public String getcardno()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.cardno;
	}
	public void setcardno(String value)
	{
	     
	     this.cardno = value;
	}
	
	public String getmpesaref()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.mpesaref;
	}
	public void setmpesaref(String value)
	{
	     
	     this.mpesaref = value;
	}
	
	public String getvouchernumbr()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.vouchernumbr;
	}
	public void setvouchernumbr(String value)
	{
	     
	     this.vouchernumbr = value;
	}



}
