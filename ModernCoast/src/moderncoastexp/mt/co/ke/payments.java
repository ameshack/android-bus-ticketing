package moderncoastexp.mt.co.ke;


import moderncoastexp.mt.co.ke.server.SeverSender;
import moderncoastexp.mt.co.ke.server.myconstants;

import org.json.JSONObject;

import com.andreabaccega.widget.FormEditText;



import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class payments extends ActionBarActivity implements OnClickListener
{
	
	 String advicetextmpesa,advicetextairtel,advicetextyu;
	 RadioButton btndesubmitmpesa,btndesubmitairtel,btndesubmitvisa,btndesubmitmaster;
	 Button b2,visasendbutton;
	 TextView edit1,editerror,edittextpayguide,orderdetail;
	 FormEditText edit, paymetphneNo, paymetIdNo;
	 String paytype;
	 View myVisalay,mobileMoneylay;
	 
	 String busname; 
	 String busdeptime;
	 String busreptime;
	 String routecode;
	 String Traveldate;
	 String busId;
     String seatlist;
	 String bizprice;
	 String fclasprice ;
	 String vipsprice;
	 String SeatPrefix,seatNumber,Bfrom,Bto,seatType,FinalPrice,CustomerClas;
	
	//private String storename,storedesc,minamount,maxamount,storephone,voucherid,storeid;
	//private	double Dminamount,Dmaxamount;
	
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
       
        
        setContentView(R.layout.payments);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
        
        
        busdeptime = getIntent().getSerializableExtra("deptime").toString();
	       busreptime = getIntent().getSerializableExtra("reptime").toString();
	       busname = getIntent().getSerializableExtra("busname").toString();
	      // routecode = getIntent().getSerializableExtra("thidate").toString();
	       Traveldate = getIntent().getSerializableExtra("datetravel").toString();
	       busId = getIntent().getSerializableExtra("busid").toString();
	       seatlist = getIntent().getSerializableExtra("seatlist").toString();
	       seatNumber = getIntent().getSerializableExtra("seatno").toString();
	       bizprice = getIntent().getSerializableExtra("bizprice").toString();
	       fclasprice = getIntent().getSerializableExtra("fclassprice").toString();
	       vipsprice = getIntent().getSerializableExtra("vipprice").toString();
	       Bfrom = getIntent().getSerializableExtra("from").toString();
	       Bto = getIntent().getSerializableExtra("to").toString();
	       seatType = getIntent().getSerializableExtra("seatype").toString();
	       
	       if(seatType.contains("Vip"))
	       {
	    	   FinalPrice=vipsprice;
	    	   CustomerClas="VIP";
	       }
	       
	       else if(seatType.contains("Fcls"))
	       {
	    	   FinalPrice=fclasprice;
	    	   CustomerClas="1st Class";
	       }
	       else{
	    	   FinalPrice=bizprice;
	    	   CustomerClas="Business Class";
	       }
        
    	PaymentDetails();
     
     final String CVN="The last 3 digit number located on the back of your card on or above your signature line";
     
     
     orderdetail = (TextView)findViewById(R.id.myorderdetail);
     orderdetail.setText(Bfrom.toUpperCase()+" - "+Bto.toUpperCase()+" "+busname+" Seat No. "+seatNumber+" "+CustomerClas+" "+busdeptime+" on "+Traveldate);
     
     myVisalay = (View)findViewById(R.id.visalayout);
     
     mobileMoneylay = (View)findViewById(R.id.mobilemoneylayout);
     
     edit1 = (TextView)findViewById(R.id.edittextviwew);
     
     editerror = (TextView)findViewById(R.id.edittextviwewerror);
     
     edittextpayguide = (TextView)findViewById(R.id.edittextpayguide);
     
     edit = (FormEditText)findViewById(R.id.edittext);
     paymetphneNo = (FormEditText)findViewById(R.id.user_phoneme);
     paymetphneNo.setText(myconstants.userphoneNO);
     
     btndesubmitmpesa= (RadioButton) findViewById(R.id.paympesa);
     btndesubmitmpesa.setOnClickListener(this);
     
     btndesubmitairtel= (RadioButton) findViewById(R.id.payairtel);
     btndesubmitairtel.setOnClickListener(this);
     
     btndesubmitvisa= (RadioButton) findViewById(R.id.payvisa);
     btndesubmitvisa.setOnClickListener(this);
     
     btndesubmitmaster= (RadioButton) findViewById(R.id.paymaster);
     btndesubmitmaster.setOnClickListener(this);
    
     
     b2 = (Button)findViewById(R.id.oktopay);
   //  b2.setBackgroundColor(Color.parseColor("#ff6700"));
     b2.setOnClickListener(this);
     
     
     
     Button  mBtnGO = (Button)findViewById(R.id.btnGO);
	 ButtonTextView myEditText = (ButtonTextView)this.findViewById(R.id.user_visacvn);
	 myEditText.setBtnOk(mBtnGO);
        mBtnGO.setOnClickListener(new View.OnClickListener() {
        	
          public void onClick(View v) {
        	  ToastMe(CVN);
        	  
        	
          }
        });
        
        VisaButton();
     
     

    advicetextmpesa="Send M-PESA "+myconstants.paymentCurrency+". "+FinalPrice+" to Pay Bill Business number 880400. Submit the Confirmation Code above"+
   "\n"+"1. Go to M-PESA on your phone"+"\n"+"2. Select Pay Bill option"+"\n"+"3. Enter Business no. 880400"+
   "\n"+"4. Leave the Account no. blank"+"\n"+"5. Enter the Amount "+myconstants.paymentCurrency+". "+FinalPrice+""
   +"\n"+"6. Enter your M-PESA PIN and Send"+"\n"+"7. You will receive a confirmation SMS from M-PESA with a Confirmation Code"
   +"\n"+"8. After you receive the confirmation SMS, enter the Confirmation Code above and click complete"
   +"\n"+"";
  
  
    advicetextairtel="Send Airtel Money "+myconstants.paymentCurrency+". "+FinalPrice+" nick-name MSHOP. Submit the Transaction ID above."+
    "\n"+"1. Go to Airtel Money option on your phone"+"\n"+"2. Select Send Money Option"+"\n"+"3. Select Enter Nick-Name Option"+
    "\n"+"4. Type the name MSHOP"+"\n"+"5. Enter the Amount "+myconstants.paymentCurrency+". "+FinalPrice+""
    +"\n"+"6. Confirm Amount and Nickname"+"\n"+"7. Enter your PIN and Send"
    +"\n"+"8. You will receive a confirmation SMS from Airtel Money with a Transaction ID"
    +"\n"+"9. After you receive the confirmation SMS, enter the Transaction ID below and click complete";
   
    
    
    advicetextyu="Send YuCash "+myconstants.paymentCurrency+". "+FinalPrice+" to Business Number 180400. Submit the TxnID above"+
    "\n"+"1. Go to yuCash menu on your phone"+"\n"+"2. Select Send Money option"+"\n"+"3. Enter Business no. 180400"+
    "\n"+"4. Enter the Amount "+myconstants.paymentCurrency+". "+FinalPrice+""+"\n"+"5. Leave the Message blank"
    +"\n"+"6. Enter your yuCash PIN, and Send"+"\n"+"7. You will receive a confirmation SMS from yuCash with a TxnID"
    +"\n"+"8. After you receive the confirmation SMS, enter the TxnID below and Click on Complete";
    
    //by default load mpesa   
    btndesubmitmpesa.setChecked(true);
    onClick(btndesubmitmpesa);
    
    }
    
	public void ToastMe(String thetext){
		Toast.makeText(this, thetext, Toast.LENGTH_LONG).show();
	}
    
	
	
	
	public void VisaButton() {
		 
		visasendbutton = (Button) findViewById(R.id.buttonvisacomp);
		
		
		visasendbutton.setText("Pay KES "+FinalPrice);
 
		visasendbutton.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				//calling visa stripe here
 
			}
 
		});
 
	}
	
    
	public void onClick(View v) {
		
if (v ==  btndesubmitmpesa)
{
	
	edit1.setText(advicetextmpesa);
	myVisalay.setVisibility(View.GONE);
	myVisalay.setVisibility(View.INVISIBLE);
	
	   b2.setVisibility(View.VISIBLE);
	    edit.setVisibility(View.VISIBLE);
	    edit.setHint("Enter M-pesa Code");
	    
	    paymetphneNo.setVisibility(View.VISIBLE);
	    paymetphneNo.setHint("Enter Phone Number");
	    
	    edittextpayguide.setVisibility(View.VISIBLE);
    
    mobileMoneylay.setVisibility(View.VISIBLE);
  
    
    TranslateAnimation slide = new TranslateAnimation(0, 0, 100,0 ); 
    slide.setDuration(1000);
    
    slide.setFillAfter(true);  
    mobileMoneylay.startAnimation(slide);
    
    paytype="Mpesa";
    
    
}


else if(v==b2)
	 
{

	editerror.setText("");
	 editerror.setVisibility(View.GONE);
	
	 
	 FormEditText[] allFields    = { paymetphneNo,edit};
	    boolean allValid = true;
     for (FormEditText field: allFields) {
         allValid = field.testValidity() && allValid;
     }

     if (allValid) {	
    	 
    	  Intent intent = new Intent(v.getContext(), LoadTicket.class);
  	    intent.putExtra("busname",busname );
  		intent.putExtra("deptime",busdeptime );
  		intent.putExtra("datetravel",Traveldate );
  		intent.putExtra("reptime",busreptime );
  		intent.putExtra("busid",busId );
  		intent.putExtra("seatlist",seatlist );
  		intent.putExtra("bizprice",bizprice );
  		intent.putExtra("fclassprice",fclasprice );
  		intent.putExtra("vipprice",vipsprice );
  		intent.putExtra("seatno",seatNumber );
  		intent.putExtra("seatype",seatType);
  		intent.putExtra("finalprice",FinalPrice);
  		intent.putExtra("mpesaref",edit.getText().toString());
  		intent.putExtra("phone",paymetphneNo.getText().toString());
  		intent.putExtra("from",Bfrom );
  		intent.putExtra("to",Bto );
  	   startActivity(intent);
    	 
     }
     
     else {
         // EditText are going to appear with an exclamation mark and an explicative message.
     }
 
    	
    

}

else if(v ==  btndesubmitairtel)
{
	    edit1.setText(advicetextairtel);
	    myVisalay.setVisibility(View.GONE);
	    myVisalay.setVisibility(View.INVISIBLE);
	    mobileMoneylay.setVisibility(View.VISIBLE);
	    
	    b2.setVisibility(View.VISIBLE);
	    edit.setVisibility(View.VISIBLE);
	    edit.setHint("Enter Airtel Code");
	    
	    paymetphneNo.setVisibility(View.VISIBLE);
	    paymetphneNo.setHint("Enter Phone Number");
	    
	    edittextpayguide.setVisibility(View.VISIBLE);
	    
	  
	    TranslateAnimation slide = new TranslateAnimation(0, 0, 100,0 ); 
	    slide.setDuration(1000);
	    
	    slide.setFillAfter(true);
	    mobileMoneylay.startAnimation(slide);
	    paytype="Airtel";
}

else if(v ==  btndesubmitvisa)
{
	   edit1.setText("");
	 mobileMoneylay.setVisibility(View.GONE);
	   mobileMoneylay.setVisibility(View.INVISIBLE);
	    myVisalay.setVisibility(View.VISIBLE);
	    
	    b2.setVisibility(View.GONE);
	    edit.setVisibility(View.GONE);
	    paymetphneNo.setVisibility(View.GONE);
	    
	    edittextpayguide.setVisibility(View.GONE);
	   
	    TranslateAnimation slide = new TranslateAnimation(0, 0, 100,0 ); 
	    slide.setDuration(1000);
	    
	    slide.setFillAfter(true);
	    mobileMoneylay.startAnimation(slide);
	    myVisalay.startAnimation(slide);
	    
	    paytype="YuCash";
}

else if(v ==  btndesubmitmaster)
{
	   edit1.setText("");
	 mobileMoneylay.setVisibility(View.GONE);
	   mobileMoneylay.setVisibility(View.INVISIBLE);
	    myVisalay.setVisibility(View.VISIBLE);
	    
	    b2.setVisibility(View.GONE);
	    edit.setVisibility(View.GONE);
	    paymetphneNo.setVisibility(View.GONE);
	    
	    edittextpayguide.setVisibility(View.GONE);
	   
	    TranslateAnimation slide = new TranslateAnimation(0, 0, 100,0 ); 
	    slide.setDuration(1000);
	    
	    slide.setFillAfter(true);
	    mobileMoneylay.startAnimation(slide);
	    myVisalay.startAnimation(slide);
	    
	    paytype="Master";
}
		
	}
	
	public void PaymentDetails()
	{
		try{
			
		/*JSONObject jObject = new JSONObject(myconstants.storedetjson);
		
		storename=jObject.getString("Storename");
		storedesc=jObject.getString("Storedesc");
		minamount=jObject.getString("Minamount");
		maxamount=jObject.getString("maxamount");
		voucherid=jObject.getString("Voucherid");
		storephone=jObject.getString("phone");
		storeid=jObject.getString("Storeid");
		
		Dminamount=jObject.getDouble("Minamount");
		Dmaxamount=jObject.getDouble("maxamount");*/
		
		TextView totalamountchuz = (TextView)findViewById(R.id.totalamount);
		//totalamountchuz.setText(myconstants.paymentCurrency+".  "+FinalPrice);
		
		totalamountchuz.setText("KES "+FinalPrice);
		
	
			    
	  }
		catch (Exception e) {
		      Log.d("InputStream", e.getLocalizedMessage());
		  }
		  
			}

	
	 public void showToast(String text){
		  Toast.makeText(this,text,
		           Toast.LENGTH_LONG).show();
		
	}

}
